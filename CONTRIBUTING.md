# Expected workflow

The expected workflow for contributors is to:
* Fork the repo
* Make any changes required
* Once the changes are complete:
  * Make sure the branch is up to date with master
  * Make sure the CI jobs are passing
  * Create a pull request

In the pull request please provide a summary of what has been changed and why,
referencing an issue may be enough if there is an open issue for the change.

Branch names should start with the issue number and then a brief relevant name,
for example: '49-update-the-docs'. The name doesn't need to be that long as
the issue number gives most of the context but should be enough that you don't
need to keep looking up the issue number to know what the branch is for. 

In a similar vein commit messages should start 'Issue #<number>: <message>'.
The commit messages should be relevant and helpful as far as possible. Squashing
commits can be good to reduce the number of commits messages like 'fix pep8 error'
but this is not a requirement.

# IDE and coding style

Pycharm is the IDE being used and some basic settings for it are checked
into the repository. Feel free to use others but please don't check in their
files.

Pep8 should be fully followed and there should be no pylint errors within
reason. The minimum acceptable average for the library is a pylint score of 9.
