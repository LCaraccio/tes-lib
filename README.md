[![build status](https://gitlab.com/LCaraccio/tes-lib/badges/main/pipeline.svg)](https://gitlab.com/LCaraccio/tes-lib/commits/main)
![pylint](https://lcaraccio.gitlab.io/tes-lib/pylint.svg)
![flake8](https://lcaraccio.gitlab.io/tes-lib/flake8.svg)

# tes-lib

The Test Event Store Library assists in performing asynchronous testing of
complex systems by providing an ordered store of events which can then be queried.

It aims to resolve timing issues and ordering issues that commonly occur in complex systems and
provide a single store to query when asserting the state of the system.

The public docs are available at https://lcaraccio.gitlab.io/tes-lib

To raise issues or to contribute see https://gitlab.com/LCaraccio/tes-lib
