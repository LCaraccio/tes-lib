#!/usr/bin/env bash

# Trap any error so we can return an error status
err=0
trap "err=1" ERR

BASEDIR="$(cd "$(dirname "$0")" && pwd)"

# Move into this folder so we know where we are
cd $BASEDIR

cd ../tes_lib

# Run the flake8 command and store the number of issues, don't let this
# command fail if there are issues
ISSUE_COUNT="$(flake8 --ignore=F403,F405,W503 --max-line-length=100 --count . 2>&1 > $BASEDIR/generated_files/flake8_output.txt)"

# If there are no issues ISSUE_COUNT wont be set so we need to set it
if [ -z "$ISSUE_COUNT" ]; then
    ISSUE_COUNT=0
fi

# Display the output if there were any
cat $BASEDIR/generated_files/flake8_output.txt

echo "Number of issues: "$ISSUE_COUNT

# Write the number of issues to file so that it can be archived and used later
echo -n $ISSUE_COUNT > $BASEDIR/generated_files/flake8_issue_count.txt


# Return non-zero if any command in the script has failed
test $err -eq 0