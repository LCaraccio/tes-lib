#!/usr/bin/env bash

# Trap any error so we can return an error status
err=0
trap "err=1" ERR

BASEDIR="$(cd "$(dirname "$0")" && pwd)"

# Move into this folder so we know where we are
cd $BASEDIR

# Remove any previously generated badges
rm -f $BASEDIR/generated_files/*.svg

# Generate the pylint badge
PYLINT_SCORE=$(cat $BASEDIR/generated_files/pylint_score.txt)
anybadge --value=$PYLINT_SCORE --file=generated_files/pylint.svg pylint

ISSUE_COUNT=$(cat $BASEDIR/generated_files/flake8_issue_count.txt)

if [ $ISSUE_COUNT -gt 0 ]; then
    anybadge --label flake8 --value $ISSUE_COUNT --file $BASEDIR/generated_files/flake8.svg -c red -m %d
else
    anybadge --label flake8 --value $ISSUE_COUNT --file $BASEDIR/generated_files/flake8.svg -c green -m %d
fi

# Return non-zero if any command in the script has failed
test $err -eq 0