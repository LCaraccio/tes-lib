#!/usr/bin/env bash

# Trap any error so we can return an error status
err=0
trap "err=1" ERR

BASEDIR="$(cd "$(dirname "$0")" && pwd)"

# cd into this folder so we know where we are
cd $BASEDIR

pushd ../docs

# Clear out the build folder
rm -rf _build/*

# Generate api
DIRS=`find ../tes_lib/ -type d`
APIBLACKLIST=()
CONFBLACKLIST=()
CONFBLACKLIST+="'_build'"
CONFBLACKLIST+=", 'Thumbs.db'"
CONFBLACKLIST+=", '.DS_Store'"
for file in `find ../tes_lib -regex .*.py`; do
  if ! grep -q $file ../ci_scripts/api_whitelist.txt; then
    CONFBLACKLIST+=", '$file'"
    APIBLACKLIST+=" $file"
  fi
done

for dir in $DIRS; do
  if ! grep -q $dir ../ci_scripts/api_whitelist.txt; then
    CONFBLACKLIST+=", '$dir'"
    APIBLACKLIST+=" $dir"
  fi
done

# Generate restructured text files
sphinx-apidoc -o ./ ../tes_lib/ $APIBLACKLIST

# Generate html
make html

popd > /dev/null

# Return non-zero if any command in the script has failed
test $err -eq 0
