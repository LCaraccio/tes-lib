#!/usr/bin/env bash

# Trap any error so we can return an error status
err=0
trap "err=1" ERR

BASEDIR="$(cd "$(dirname "$0")" && pwd)"

# Move into this folder so we know where we are
cd $BASEDIR

cd ..

# Run pylint script
ci_scripts/pylint_checker.py tes_lib

# Return non-zero if any command in the script has failed
test $err -eq 0