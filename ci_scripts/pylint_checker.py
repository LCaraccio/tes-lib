#! /usr/bin/env python3
"""
Run pylint on a module and make sure the score is above or equal to a threshold
"""
import os
import sys
from pylint import lint

THRESHOLD = 9

if len(sys.argv) < 2:
    raise RuntimeError("Module to evaluate needs to be the first argument")

run = lint.Run([sys.argv[1]], do_exit=False)
score = run.linter.stats.global_note

# Get the dir this script is in
script_dir = os.path.dirname(os.path.realpath(__file__))

score_file = open(os.path.join(script_dir, "generated_files", "pylint_score.txt"), "w")
score_file.write(f"{score:.2f}")
score_file.close()

if score < THRESHOLD:
    print("Average score: {:.2f} Minimum Threshold: {:.2f}".format(score, THRESHOLD))
    sys.exit(1)
