#!/usr/bin/env bash
# Runs all the tests without coverage

# Trap any error so we can return an error status
err=0
trap "err=1" ERR

BASEDIR="$(cd "$(dirname "$0")" && pwd)"

# Move into this folder so we know where we are
cd $BASEDIR

# Add the relevant folders to the path
export PYTHONPATH="${PYTHONPATH}:$BASEDIR/../"

# Move to the test folder
pushd ../test

# Run the tests
pytest -v

popd

# Return non-zero if any command in the script has failed
test $err -eq 0
