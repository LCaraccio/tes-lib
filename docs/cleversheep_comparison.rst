Comparison with CleverSheep Event Store
=======================================

tes-lib is based on the event store within `CleverSheep <https://lcaraccio.gitlab.io/clever-sheep/api/index.html>`_.

It aims to solve a number of issues with the event store in CleverSheep, these are as follows:

* tes-lib event store runs as a separate process:
    * Having `time.sleep` in a test will not stop events being added to the tes-lib event store
    * You cannot block events being added to the store if your test doesn't yield control back
      to the test runner often enough
    * Mocks do not have to interact with or be part of the test runner process so there is no need
      to start them under a repeating timeout further reducing chances of blockage
* Events are added via a webserver:
    * This makes it easier to add events from non-python programs and python programs running as
      separate processes. There is no need to do things like receive data, write it to a log file
      and then have a python program watching the log file. The process receiving the data can put
      the events directly into the store (Assuming it is a test process not a part of the
      production system).
* The event store is not limited in size or age
    * There will be no issues with events being silently removed due to store length
    * There will be no issues with events being silently removed due to event age
    * There will be no issues with chop
* The expectation system is simple and extensible
    * There should be no need to have a litany of custom expect functions, each with a slightly
      different check/match/err function.
* The TestEvent system makes it clear what fields an event has
    * Rather than having to find the process adding the event you can simply look at the event class
      to figure out what fields it has
    * The requirement of an event source should also make it hard to get events from the wrong source
