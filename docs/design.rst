Design
------

.. image:: images/teslib_overview.png

1. The event store runs as its own process. The reason for doing this is so
   that adding events and expecting events can be done separately without blocking each other.
2. There is also be another process running that acts as a webserver. The sole
   function this webserver has is to allow events to be added to the store. The
   reason this exists is to make adding events from non-python programs or separate
   python programs trivial and standardised.
3. The library provides functions for expecting events and setting up/tearing
   down the library. The webserver and event store processes are spawned
   from the test runner process.

The webserver is written using the builtin `BaseHTTPRequestHandler`
rather than an external library. This is because the use case is so simple. This can be changed at
a later point in time if a reason to do so becomes apparent.
