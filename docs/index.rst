.. tes-lib documentation master file

Welcome to the Test Event Store Library documentation!
======================================================

The Test Event Store Library (tes-lib) provides assistance in writing asynchronous
tests for complex systems. In does this by storing events in an independent
event store which can then be queried at a later point in time.

To raise issues or contribute see the code `repository <https://gitlab.com/LCaraccio/tes-lib>`_

Introduction
------------

The goal of the library is to make testing complex systems easier. It does this by providing a consistent way to store
observations and events that happen during a test, as well as a unified way to assert the expected observations or
events have occurred.

Why is it needed
----------------

In large or complex systems we often need to examine state and keep a record of a wide array of interactions to make
sure the system is behaving as expected.

These tend to fall into two categories, events triggered by the system under test (SUT) and observations of output
produced by the SUT. For example:

* packets on a network connection
* items on a queue
* files on a shared file system be that S3, SFTP, NFS
* calls to mock APIs, often webservers
* metrics
* logs

Aspects of higher level tests that present challenges are:

* the order of events changing between runs, due to timing or non deterministic data structures
* not being able to stop the system to examine state
* processes exiting before you are able to inspect their state
    * programs crashing, mocks exiting, services being spun down after load reduction
* missing state due to the transition being too quick to detect, having happened and reverted by
  the time polling begins
    * udp connection state changing
    * metric gauges increasing and de-creasing again before being inspected

When checking if a behaviour has occurred, it is always best to poll repeatedly rather than put a delay in followed by
a single check. This makes tests more robust to timing changes, and, reduces run times as we can detect the
behaviour occurring quite rapidly after it happens.

This gives us sets of mocks that need to not only respond to the SUT, but also need to keep track of the calls they
receive and provide a way to access this information later.

We also require test services observing the SUT for items such as metric values, to store a record of their results, and
again provide a way to retrieve this information.

This tends to result in:

* poll loops all over the testing code
* mocks and observers that are overly complex
* test code that has to know how to query a wide array of mocks and observers

How does tes-lib help
---------------------

Using tes-lib, each mock or observer uses a common way to record the relevant information, and the test has a
common way to query that information, without having to know the details of how it was generated.

This is done by creating events which are placed in a central store, the event store, that can later br queried by the
test. These could be one to one events, such as an event for each call made to a webserver, or, they could be summaries
such as the total number of packets received on a network connection reported once a second.

This simplifies mocks and observers, they no longer need to store a record of calls made or observations seen, they also
don't have to provide methods for access this information later.

The test code is also simplified, not needing to understand how to access different mocks or observers, it only needs
to interact with the event store.

Some additional benefits of using the library:

* less poll loops in your test code to check if something has occurred, there is one poll loop and it is in the library
* timing issues like starting to check after an event happens go away, as we have the history of system state in the
  event store rather than only having access to the current state
* the event store can give a view of what was going on in the system over time, which can be useful for debugging
* data is not lost when a mock or observer stops

Installation
------------

.. code-block:: bash

    pip install tes-lib

Documentation
-------------

.. toctree::
   :name: mastertoc
   :maxdepth: 2

   tutorials
   library_features
   release_notes
   rest_api
   design
   performance
   cleversheep_comparison

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
