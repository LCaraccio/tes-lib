Library Features
================

.. _expectation-failure-reason-debugging:

Expectation Failure Reason Debugging
------------------------------------

When an `expect_event` call fails the library will check if expectations for the `event_type` and `event_source` fields
are present. If one or both of these are present the library will use these fields to pre-filter the events in the
event store, it will then run through the remaining expectations for this filtered list and track which of the remaining
expectations failed for each event and why.

This is to make debugging easier when an `expect_event` call fails, rather than having to compare every expected field
for an event with that of the values in the event store, the user is presented with the most likely events and the
expectations that failed for them. This quickly highlights simple errors such field name mismatches and incorrect data
types.

For example:

.. code-block:: python

    import operator
    import tes_lib

    import logging
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(__name__)


    def main():
        tes_lib.initialise()
        tes_lib.reset_event_store()

        tes_lib.add_raw_event(
            event_source="example", event_type="a", additional_event_params={"q": 1, "z": "A"}
        )
        tes_lib.add_raw_event(
            event_source="example", event_type="a", additional_event_params={"q": 2, "z": 4}
        )
        tes_lib.add_raw_event(
            event_source="example", event_type="b", additional_event_params={"q": 3, "z": "C"}
        )
        tes_lib.add_raw_event(
            event_source="example", event_type="c", additional_event_params={"q": 4, "z": "D"}
        )

        expectations = [
            tes_lib.Expectation("event_source", operator.eq, "example"),
            tes_lib.Expectation("event_type", operator.eq, "a"),
            tes_lib.Expectation("q", operator.eq, 2),
            tes_lib.Expectation("z", operator.eq, "B"),
        ]

        try:
            tes_lib.expect_event(expectations)
        finally:
            tes_lib.cleanup()


    if __name__ == "__main__":
        main()

Would produce the following output:

.. code-block:: bash

    INFO:root:Starting up test event store
    INFO:root:Reset Event Store Called
    INFO:root:***** Logging Extended Expectation Failure Debug *****
    INFO:root:event_id: 0, {'event_source': 'example', 'event_type': 'a', 'q': 1, 'z': 'A'}
    INFO:root:    ExpectationFailure: q eq('2': int) got '1': int
    INFO:root:    ExpectationFailure: z eq('B': str) got 'A': str
    INFO:root:event_id: 1, {'event_source': 'example', 'event_type': 'a', 'q': 2, 'z': 4}
    INFO:root:    ExpectationFailure: z eq('B': str) got '4': int
    INFO:root:***** End Of Extended Expectation Failure Debug *****
    INFO:root:***** Logging Event Store *****
    INFO:root:event_id: 0, {'event_source': 'example', 'event_type': 'a', 'q': 1, 'z': 'A'}
    INFO:root:event_id: 1, {'event_source': 'example', 'event_type': 'a', 'q': 2, 'z': 4}
    INFO:root:event_id: 2, {'event_source': 'example', 'event_type': 'b', 'q': 3, 'z': 'C'}
    INFO:root:event_id: 3, {'event_source': 'example', 'event_type': 'c', 'q': 4, 'z': 'D'}
    INFO:root:***** End Of Event Store *****
    INFO:root:Shutting down test event store

From this we can see that only the events with `event_type` 'a' were logged in the "Extended Expectation Failure Debug"
output. We can also see that the first event it compared had two fields that did not match, the second had one where
the types of the values also differed.

This behaviour can be turned off globally as part of the `initialise` call if desired, it can also be turned on/off
explicitly in each `expect_event` and `get_event` call. 

The above would also output an exception like the following:

.. code-block:: bash

    tes_lib.errors.ExtendedEventNotFoundError: Failed to find an event for the 4 expectations provided.
    Expected event source: example.
    Expected event type: a.

    Event ID: 0
        ExpectationFailure: q eq('2': int) got '1': int
        ExpectationFailure: z eq('B': str) got 'A': str
    Event ID: 1
        ExpectationFailure: z eq('B': str) got '4': int

    See the logging output for full details