Library Performance
===================

Some performance testing has been done on the library simply to give a some idea of it's capability
and to see if there are any obvious issues.

Discussion
----------

The conclusions that can be drawn from the results are as follows:

* The number of events in the store has no impact on the time taken to insert an event
* More event fields has a small impact on the time taken to expect an event
* More event fields has a larger impact on insertion time than large fields
    * Probably due to needing to json serialize/deserialize each field
* The number of processes adding events to the store has minimal impact on performance
* The order and number of expectations can make a difference - if performance isn't good enough:
    * Remove any redundant expectations - for example if there is only one test source don't check it
    * Order the expectations such that the least likely to match expectation is first and the most likely expectation is last

A lot of the number of fields and events are somewhat unlikely. A more realistic setup would be
1,000 events with 10 fields spread across 10s of seconds. The library could easily cope with this
and the time to expect event in this setup should be 1s of milliseconds. Given this the currently
library performance will be considered good enough unless some evidence to the contrary is
found.

Raw Results
-----------

.. note::  The timing was done in python so the timings may not be exact.

.. note::  All times are in seconds.

.. note::  The testing was performed on v0.0.1 on the 27th February 2020.

Single Sender 2 fields
######################

Time for a single process to add events, each event consists of a source and type only.

+------------------+------------------------+-------------------+---------------------------+
| Number of Events | Average Insertion Time | Events Per Second | Time to expect last event |
+==================+========================+===================+===========================+
| 1,000            | 1.875                  | 533               | 0.001                     |
+------------------+------------------------+-------------------+---------------------------+
| 10,000           | 18.714                 | 534               | 0.004                     |
+------------------+------------------------+-------------------+---------------------------+
| 100,000          | 186.427                | 536               | 0.040                     |
+------------------+------------------------+-------------------+---------------------------+

Single Sender Extra fields
##########################

Time for a single process to add events with differing number of additional event fields. Note
in each test 1,000 events were added to the store.

+-------------------------+------------------------+-------------------+---------------------------+
| Additional Event Fields | Average Insertion Time | Events Per Second | Time to expect last event |
+=========================+========================+===================+===========================+
| 100                     | 2.053                  | 487               | 0.001                     |
+-------------------------+------------------------+-------------------+---------------------------+
| 1,000                   | 3.621                  | 276               | 0.002                     |
+-------------------------+------------------------+-------------------+---------------------------+
| 10,000                  | 21.817                 | 45                | 0.004                     |
+-------------------------+------------------------+-------------------+---------------------------+

Single Sender Large Data Fields
###############################

Time for a single process to add events with a blob field of different size. Note
in each test 1,000 events were added to the store.

+--------------------+------------------------+-------------------+---------------------------+
| Size Of Blob Field | Average Insertion Time | Events Per Second | Time to expect last event |
+====================+========================+===================+===========================+
| 1kb                | 1.975                  | 506               | 0.001                     |
+--------------------+------------------------+-------------------+---------------------------+
| 10kb               | 2.767                  | 361               | 0.001                     |
+--------------------+------------------------+-------------------+---------------------------+
| 100kb              | 11.659                 | 85                | 0.001                     |
+--------------------+------------------------+-------------------+---------------------------+

Multiple Senders
################

Time for a n processes to add events. Note in each case the total events added was 10,000.

+-------------------+------------------------+-------------------+---------------------------+
| Number of Senders | Average Insertion Time | Events Per Second | Time to expect last event |
+===================+========================+===================+===========================+
| 2                 | 18.840                 | 530               | 0.005                     |
+-------------------+------------------------+-------------------+---------------------------+
| 3                 | 19.078                 | 524               | 0.005                     |
+-------------------+------------------------+-------------------+---------------------------+
| 4                 | 19.207                 | 520               | 0.005                     |
+-------------------+------------------------+-------------------+---------------------------+

Number of expectations
######################

Time to find the last event in the event store when there are 10,000 events in the store. Note
each test had an expectation that only matched the last event along with n additional expectations
that matched every event.

+------------------------------+---------------------------+
| Number of extra expectations | Time to expect last event |
+==============================+===========================+
| 0                            | 0.004                     |
+------------------------------+---------------------------+
| 1                            | 0.007                     |
+------------------------------+---------------------------+
| 2                            | 0.009                     |
+------------------------------+---------------------------+
| 3                            | 0.012                     |
+------------------------------+---------------------------+
| 4                            | 0.014                     |
+------------------------------+---------------------------+
| 5                            | 0.017                     |
+------------------------------+---------------------------+

As you can see each additional field added 0.003s to the time. If the expectations only matched
a subset of the events, for example 10% they had a negligible impact.

The other thing to note is that the order of expectations matter - with the 5 additional expectations
if we move the expectation that only matches the last event to be first then the time drops back to
0.004 seconds.

System Info
-----------

OS: CentOS 7.7.1908

RAM: 16GB

CPU: 4 Core