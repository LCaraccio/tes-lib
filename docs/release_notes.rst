Release Notes
=============

v1.1.3
------

Bug Fixes
#######

* Fix an issue introduced in v1.1.2 where using builtins as comparison operators would cause an exception to occur.

v1.1.2
------

Changes
#######

* Add the ability to use comparison functions that only take 1 parameter which is the actual value for the field.

v1.1.1
------

Changes
#######

* Fix an issue where a TypeError could be returned from `expect_event` and similar when comparison functions that don'that
  support None types were used, such as `operator.gt`. It will now be treated as an expectation failure.

v1.1.0
------

Changes
#######

* The default TestEvent class has been updated to handle storing and generating expectations for fields transparently.
  The `__init__` and `get_expectations` function signatures have been updated in a non breaking manner for this change.

v1.0.0
------

Changes
#######

* Added new formatting for exceptions to improve the readability on the command line when an error occurs.
* The ability to specify custom fields for partial matching when using the extended
  debugging output has been removed due to adding complexity for no current value.
* Removed the timeout parameter on the `reset` method as it was unused, override the
  request timeout in the `initialise` call to change the timeouts used in the library.

Bug Fixes
#########

* Fix an issue with the request timeout and poll interval being supplied in place of each
  other when overridden in the `initialise` call.

v0.1.2
------

Changes
#######

* Fix a bug in the new close match lookup behaviour.

v0.1.1
------

Changes
#######

* Add behaviour to look for close matches to an event on expectation failure.

v0.1.0
------

Changes
#######

* Add the ability to set default values for:
    * the function called on failure
    * the request timeout to the event store
    * the poll interval for repeated requests
    * the maximum poll time when retrieving an event be that via get or expect
    * the maximum poll time when checking an event is not in the event store
* The `timeout` parameter has been renamed in a number of methods to `maximum_poll_time`
* `maximum_poll_time` is now optional on `dont_expect_event`

v0.0.4
------

Changes
#######

* Return a useful error if the list of expectations is None or empty.
* Keep the event fields first when logging the events in the event store.

v0.0.3
------

Changes
#######

* Add the `get_event` method.
* Fix pylint and mypy issues.
* Documentation improvements.

v0.0.2
------

Bug Fixes
#########

* Fixed an issue with the library not stating it requires `requests` in the "setup.py".

Changes
#######

* Added a number of filters and controls for logging the event store
* Added a log full event store option
* Documentation improvements.

v0.0.1
------

Initial release of the library.