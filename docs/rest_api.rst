.. _add_event_webserver:

Add Event Rest API
==================

By default the add event webserver runs on 127.0.0.1:8080.

Webserver Ready
---------------
  Provides an endpoint for testing if the add event webserver is ready. This is called by the library as part of the
  initialise call.

* **URL**

  * /ready

* **Method:**

  * `GET`

* **Success Response:**

  * **Code:** 200

Add Event
---------
  Add an event to the event store.

* **URL**

  * /add

* **Method:**

  * `POST`

* **Data Params**

  **Content:**

.. code-block:: JSON

    {
        "event_source": [str],
        "event_type": [any],
        "user_field_1": [any],
        ...
        "user_field_n": [any],
    }

* **Success Response:**

  * **Code:** 200

* **Error Response:**

  * | **Code:** 400 BAD REQUEST
    | **Content:** `"<Error Description>"`

