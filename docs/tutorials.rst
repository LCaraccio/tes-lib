Tutorials
=========

These tutorials use pytest as the example test runner but any python based test runner can be used.

Library Setup and Teardown
--------------------------

.. code-block:: python

    import tes_lib
    import pytest

    @pytest.fixture(scope='session', autouse=True)
    def event_store():
        tes_lib.initialise()
        yield
        tes_lib.cleanup()

    @pytest.fixture(scope='function', autouse=True)
    def clear_event_store(event_store):
        tes_lib.reset_event_store()

The `initialise` and `cleanup` functions should always be called as a pair. This can be done
per test, per module or once for the whole run.

`reset_event_store` is expected to be called on a per-test basis. It should not be called before `initialise` has been called.
This will clear the event store. If for some reason this fails it will check the webserver and event
store processes are running, starting them if they are not.

`initialise` can be used to configure a number of aspects of the library such as the default function to use on failure,
default timeouts or the port and ip address used by the webserver.

Users will also need to configure the root logger to info level for normal library output or debug for more detailed information.

Adding Events
-------------

All events are added to the event store via the add event web server, see: :ref:`add_event_webserver`.

If adding events from a Python program there are two helper functions available to help with interacting with this
webserver. These helper functions can be given an IP and port if the defaults are not being used for the add event webserver.

The first is `add_event` which expects to be given a subclass of `TestEvent`:

.. code-block:: python

    import tes_lib

    class UserEvent(tes_lib.TestEvent):
        EVENT_TYPE = "UserEvent"
        def __init__(self, event_source, **kwargs):
            super().__init__(event_source, self.EVENT_TYPE, **kwargs)

    def add_my_event():
        tes_lib.add_event(
            UserEvent("My Log Watcher A", field1=1, field2=2, field3=3)
        )

The second is the `add_raw_event` function which takes the event source, event type
and a dictionary of additional fields.

.. code-block:: python

    def add_my_event():
        tes_lib.add_raw_event(
            "My Log Watcher A", "UserEvent", {field1: 1, field2: 2, field3: 3}
        )

The first option is the recommended approach if the service adding events is a python service, this is to make sure any
required fields are specified, see the `Custom Event Classes`_ section for more.

Some examples of programs that might be generating these events are:

* Log file watchers
* Test receivers that terminate, for example, UDP or TCP connections
* Programs receiving or polling for diagnostic information such as metrics

Events can be anything. They can be generated for each trigger or aggregate a number of triggers
over a time period. For example a test service terminating a tcp connection could generate an event for each packet it
receives or a count of the packets it has received each second. This would depend on the use case, data volume and if we
desire to inspect the packet payloads. 

Expecting Events
----------------

Once we have a method for storing events in the events store, we now want to be able to add expectation in our test code
to make sure the events we would expect have occurred. There are a number of helper functions in the library, the
main one being `expect_event`. All of them take a list of `Expectations`.

See the :ref:`modindex` for a full list.

An `Expectation` is formed of the name of the field to compare, a comparison operator and
a comparison value. This is displayed in logging by the library in the form 
``<field_name> <comparison_function>(<comparison_value>: <comparison_value_type>)``. For example ``event_source eq('My Log Watcher A': str)``.

A list of expectations can be can be created and passed directly, for example:

.. code-block:: python

    import operator
    import tes_lib


    class UserEvent(tes_lib.TestEvent):
        EVENT_TYPE = "UserEvent"

        def __init__(self, event_source, **kwargs):
            super().__init__(event_source, self.EVENT_TYPE, **kwargs)


    def main():
        tes_lib.initialise()

        try:
            for i in range(5):
                tes_lib.add_event(UserEvent("My Log Watcher A", field1=i, field2=i * 2, field3=i * 3))

            expectations = [
                tes_lib.Expectation("field1", operator.eq, 1),
                tes_lib.Expectation("field2", operator.le, 6),
            ]
            event = tes_lib.expect_event(expectations)
            print(event)
            # Output: 
            # {
            #     'event_source': 'My Log Watcher A',
            #     'event_type': 'UserEvent',
            #     'field1': 1, 'field2': 2,
            #     'field3': 3,
            #     'event_id': 1
            # }
        finally:
            tes_lib.cleanup()


    if __name__ == "__main__":
        main()


The `expect_event` function will find the **first** event that matches all the expectations given.
If no event is found before the maximum poll time, the events in the event store will be logged and an
exception raised.

Fields on events in the event store will be ignored for comparison purposes if there's no expectation related to them.

Custom Event Classes
~~~~~~~~~~~~~~~~~~~~

Event classes help with producing expectations in a number of ways, they provide:

* A place to keep constants such as the event type
* Expectation generation for all fields provided
* A place to add expectations related to business logic
* They help to insure that the event source and event type are always provided along with any other required fields

The base `TestEvent` accepts kwargs and will generate expectations for each field provided.

.. code-block:: python

    import tes_lib

    class UserEvent(tes_lib.TestEvent):
        EVENT_TYPE = "UserEvent"
        def __init__(self, event_source, **kwargs):
            super().__init__(event_source, self.EVENT_TYPE, **kwargs)

    ue = UserEvent("My Log Watcher A", field1=1, field2=2, field3=3)
    print(ue.get_expectations())
    # Output:
    # [
    #     event_source eq('My Log Watcher A': str),
    #     event_type eq('UserEvent': str), field1 eq('1': int),
    #     field2 eq('2': int),
    #     field3 eq('3': int)
    # ]

    # Expectations are only generated for the fields provided.
    ue2 = UserEvent("My Log Watcher B", field1=1, field2=2)
    print(ue2.get_expectations())
    # Output:
    # [
    #     event_source eq('My Log Watcher A': str),
    #     event_type eq('UserEvent': str), field1 eq('1': int),
    #     field2 eq('2': int)
    # ]

This behaviour means that field names do not have to be specified on the test event type, this can be useful to reduce
maintenance overhead when data structures are likely to change or the structure of the events is dynamic.

This can also be useful when an event represents a data structure defined separately and we don't want to duplicate it
such as a GraphQL schema.

There may be cases where we want to specify some or all of the field names, it makes it easier for callers to know what
fields are on the events and can help to ensure certain fields are always provided.

.. code-block:: python

    import operator
    import tes_lib

    class UserEvent(tes_lib.TestEvent):
        EVENT_TYPE = "UserEvent"
        def __init__(self, event_source, field1, **kwargs):
            kwargs["field1"] = field1
            super().__init__(event_source, self.EVENT_TYPE **kwargs)

Depending on the complexity of the data type and business logic involved we may want to override the `get_expectation`
method to add additional checks.

.. code-block:: python

    import operator
    import tes_lib

    class UserEvent(tes_lib.TestEvent):
        EVENT_TYPE = "UserEvent"
        def __init__(self, event_source, **kwargs):
            super().__init__(event_source, self.EVENT_TYPE **kwargs)

        def get_expectation(self, custom_operators=None):
            expectations = super().get_expectations(custom_operators=custom_operators)

            # Additional checks if required related to the domain
            expectations.append(
                tes_lib.Expectation("field2", operator.gt, self.field1)
            )
            expectations.append(
                tes_lib.Expectation("field3", operator.gt, self.field2)
            )

            return expectations

As a complete example of creating events to use in generating expectations:

.. code-block:: python

    import operator
    import tes_lib


    class UserEvent(tes_lib.TestEvent):
        EVENT_TYPE = "UserEvent"

        def __init__(self, event_source, **kwargs):
            super().__init__(event_source, self.EVENT_TYPE, **kwargs)


    def main():
        tes_lib.initialise()

        try:
            for i in range(5):
                tes_lib.add_event(UserEvent("My Log Watcher A", field1=i, field2=i * 2, field3=i * 3))
       
            # Example of a simple expect event where we just want all the fields to match exactly
            expected_event = UserEvent(
                "My Log Watcher A",
                field1=1,
                field2=2,
                field3=3,
            )
            event = tes_lib.expect_event(expected_event.get_expectations())
            print(event)
            # Output:
            # {
            #     'event_source': 'My Log Watcher A',
            #     'event_type': 'UserEvent',
            #     'field1': 1,
            #     'field2': 2,
            #     'field3': 3,
            #     'event_id': 1
            # }

            # Example of overriding the default matching logic
            expected_event2 = UserEvent(
                "My Log Watcher A",
                field2=7,
                field3=7,
            )
            event2 = tes_lib.expect_event(
                expected_event2.get_expectations(
                    custom_operators={"field2": operator.lt, "field3": operator.gt}
                )
            )
            print(event2)
            # Output:
            # {
            #     'event_source': 'My Log Watcher A',
            #     'event_type': 'UserEvent',
            #     'field1': 3,
            #     'field2': 6,
            #     'field3': 9,
            #     'event_id': 3
            # }
        finally:
            tes_lib.cleanup()


    if __name__ == "__main__":
        main()


Event Removal From The Store
----------------------------

When an event that has been expected is found it is returned and removed from the event store automatically. Events can
also be deleted using the `delete_all_matching_events` function.

If you wish to retrieve an event without removing it use `get_event` or `get_all_matching_events` instead.

A history of all events seen, and if they have been expected, deleted or are simply in the store is kept, and can be
accessed using `log_full_event_store`. Events are only permanently removed when `reset_event_store` is called.

Handling Expectation Failures
-----------------------------

When we expect an event and fail to find it, the library will, by default, do two things:

1) attempt to find events which partially matched and log information about why they were not complete matches
2) call the `on_failure` function, the default function will log the content of the event store

For more on the partial matching and failure logging see :ref:`expectation-failure-reason-debugging`

If a more specific behaviour over what happens when an expectation fails is desired, we can alter
the function called on failure. As an example, if we wanted to exclude a particularly large field
'blob' from being logged, we could do the following:

.. code-block:: python

    import tes_lib

    def my_test():
        def exclude_blob():
            tes_lib.log_event_store(fields_to_exclude=["blob"])

        event = tes_lib.expect_event([<Expectations>], on_failure=exclude_blob)

The `log_event_store` function can be configured to:

* specify which fields are logged
* exclude fields from being logged (mutually exclusive with specifying which fields should be logged)
* specify the field to use to order the events
* filter which events are logged using expectations as filters

This gives fine grained control to help when trying to debug issues by reducing the amount of
information being output.

There is also the option to log the "full" event store using `log_full_event_store`. This will log
all events including those that we removed after expecting them or deleted. This again may be
useful when trying to debug issues with tests.

Operators
---------

The range of `operators <https://docs.python.org/3/library/operator.html>`_ provided by the built in
module should be sufficient for the vast majority of cases.

If there are cases where it isn't, a custom operator can be used. As an example consider the following:

.. code-block:: python

    import tes_lib


    def range_operator(value, min_max):
        if min_max[0] < value < min_max[1]:
            return True

        return False


    def test_custom_operator(reset_event_store):
        tes_lib.add_raw_event("test_harness", "LibraryTestEvent", {"duration": 63})

        event = tes_lib.expect_event(
            [tes_lib.Expectation("duration", range_operator, (60, 70))]
        )
        assert event is not None

Here we have defined a custom operator that checks a value is within a range. Operators in general are expected
to take two parameters, the first will be the actual value of the field, the second is a value to compare against.
If multiple values are required for the comparison a tuple can be used as in this example.

The value to compare against can be skipped if desired but in this instance the comparison function must only take
one argument.

.. code-block:: python

    import tes_lib


    def word_comparison(value):
        if value == "Hello" or value == "World"
            return True

        return False


    def test_custom_operator(reset_event_store):
        tes_lib.add_raw_event("test_harness", "LibraryTestEvent", {"greeting": "Hello"})

        event = tes_lib.expect_event(
            [tes_lib.Expectation("greeting", word_comparison)]
        )
        assert event is not None

.. note:: Passing `None` is not equivalent to passing no value

.. warning:: The custom operator cannot be a local function - this will cause pickling of the function to fail
