"""Helper functions used to perform formatting of messages"""


def indentation(depth: int) -> str:
    """Helper function to keep number of spaces inserted when formatting lines consistent"""
    return "  " * depth
