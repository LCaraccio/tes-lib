# Testing

The tests are written using pytest. They are split into two categories, library and unit.
The library tests cover the whole library and are black box tests, the unit tests are a mix of
traditional unit tests and tests that cover a single process.

It is expected that the library tests may be supersets of multiple unit tests, they both still
offer value as the library tests will cover integration and will not test that many combinations
of values/operations. The unit tests will be more rigorous and will help
to narrow down where issues are when things break. 