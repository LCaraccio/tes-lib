import time
import tes_lib
from .support import UserEvent


def test_add_event(reset_event_store):
    """
    Test adding an event to the store
    """
    user_event = UserEvent("test_harness", "LibraryTestEvent", 27, "bob", ["fred", "dave"])
    tes_lib.add_event(user_event)

    events = tes_lib.get_all_events()

    for _ in range(5):
        if len(events) == 1:
            break

        time.sleep(0.1)
        events = tes_lib.get_all_events()

    assert len(events) == 1

    event = events[0]
    assert event["field1"] == user_event.field1
    assert event["field2"] == user_event.field2
    assert event["field3"] == user_event.field3
