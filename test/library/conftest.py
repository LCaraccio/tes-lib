import pytest
import tes_lib
import logging

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)


@pytest.fixture(scope="module")
def setup_test_lib():
    try:
        tes_lib.initialise()
    except RuntimeError as exc:
        tes_lib.cleanup()
        raise exc
    yield
    tes_lib.cleanup()


@pytest.fixture(scope="function")
def reset_event_store(setup_test_lib):
    tes_lib.reset_event_store()
