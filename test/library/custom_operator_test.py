import tes_lib


def range_operator(value, min_max):
    if min_max[0] < value < min_max[1]:
        return True

    return False


def test_custom_operator(reset_event_store):
    """
    Test comparing complex data structures with a custom operator
    """
    # Add a complex event
    event_source = "test_harness"
    event_type = "LibraryTestEvent"

    tes_lib.add_raw_event(event_source, event_type, {"duration": 63})

    import time

    time.sleep(0.5)

    event = tes_lib.expect_event([tes_lib.Expectation("duration", range_operator, (60, 70))])
    assert event is not None
