import operator
import tes_lib


def test_delete_matching_events(reset_event_store):
    """
    Test deleting matching events from the store
    """
    customer_events = 23
    for i in range(customer_events):
        event_fields = {
            "id": i,
            "age": i * 2,
        }

        tes_lib.add_raw_event("test_harness", "Customer", event_fields)

    vendor_events = 23
    for i in range(vendor_events):
        event_fields = {
            "id": i,
            "products": i * 5,
        }

        tes_lib.add_raw_event("test_harness", "Vendor", event_fields)

    expectations = [
        tes_lib.Expectation("id", operator.eq, 3),
        tes_lib.Expectation("event_type", operator.eq, "Customer"),
    ]
    expected_events_removed = 1
    total_events_removed = expected_events_removed
    actual_events_removed = tes_lib.delete_all_matching_events(expectations)
    assert expected_events_removed == actual_events_removed

    events = tes_lib.get_all_events()
    assert len(events) == customer_events + vendor_events - total_events_removed

    expectations = [
        tes_lib.Expectation("products", operator.le, 25),
    ]
    expected_events_removed = 6
    total_events_removed += expected_events_removed
    actual_events_removed = tes_lib.delete_all_matching_events(expectations)
    assert expected_events_removed == actual_events_removed

    events = tes_lib.get_all_events()
    assert len(events) == customer_events + vendor_events - total_events_removed

    expectations = [
        tes_lib.Expectation("event_type", operator.eq, "Vendor"),
    ]
    expected_events_removed = 17
    total_events_removed += expected_events_removed
    actual_events_removed = tes_lib.delete_all_matching_events(expectations)
    assert expected_events_removed == actual_events_removed

    events = tes_lib.get_all_events()
    assert len(events) == customer_events + vendor_events - total_events_removed
