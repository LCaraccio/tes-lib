import tes_lib
import operator
import pytest
import time


def test_expect_event(reset_event_store):
    """
    Test expecting an event is not in the store
    """
    expectations = [tes_lib.Expectation("event_source", operator.eq, "TestHarness")]
    tes_lib.dont_expect_event(expectations, maximum_poll_time=0.5)

    event_fields = {"animal": "Cat", "name": "TinyCat", "colour": "Black"}

    tes_lib.add_raw_event("TestHarness", "TestEventC", event_fields)

    # Make sure there's some time to actually add the event
    time.sleep(0.1)

    with pytest.raises(tes_lib.UnexpectedEventFoundError):
        tes_lib.dont_expect_event(expectations, 0.5)
