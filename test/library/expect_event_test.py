import tes_lib
import operator
from .support import UserEvent


def test_expect_event(reset_event_store):
    """
    Test expecting an event
    """
    # Add 3 events
    event_source = "test_harness"
    event_type = "LibraryTestEvent"

    tes_lib.add_event(UserEvent(event_source, event_type, 27, "bob", ["fred", "dave"]))

    tes_lib.add_event(UserEvent(event_source, event_type, 16, "cat", [5, 2, 7]))

    tes_lib.add_event(UserEvent(event_source, event_type, 20, "house", ["a", "b", "c"]))

    # Expect an event where field1 = 20
    event = tes_lib.expect_event([tes_lib.Expectation("field1", operator.eq, 20)])
    assert event is not None
