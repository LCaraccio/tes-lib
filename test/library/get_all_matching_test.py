import operator
import tes_lib
from .support import CustomerEvent, VendorEvent


def test_delete_matching_events(reset_event_store):
    """
    Test deleting matching events from the store
    """
    customer_events = 23
    for i in range(customer_events):
        tes_lib.add_event(CustomerEvent("test_harness", "Customer", id=i, age=i * 2))

    vendor_events = 23
    for i in range(vendor_events):
        tes_lib.add_event(VendorEvent("test_harness", "Customer", id=i, products=i * 5))

    expectations = [
        tes_lib.Expectation("id", operator.ge, 14),
    ]
    expected_events = 18
    actual_events = tes_lib.get_all_matching_events(expectations)
    assert len(actual_events) == expected_events
