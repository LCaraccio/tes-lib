import os
import requests
import tes_lib


def test_webserver_port():
    """Test changing the port the webserver runs on"""
    try:
        alt_port = 8081
        tes_lib.initialise(port=alt_port)

        url = "http://{}:{}/ready".format("127.0.0.1", alt_port)
        response = requests.get(url=url)
        assert response.status_code == requests.codes.ok
    finally:
        tes_lib.cleanup()


def test_werbserver_ip():
    """Test changing the ip address the webserver runs on"""
    try:
        alt_ip = "127.0.0.2"
        tes_lib.initialise(ip_address=alt_ip)

        url = "http://{}:{}/ready".format(alt_ip, 8080)
        response = requests.get(url=url)
        assert response.status_code == requests.codes.ok
    finally:
        tes_lib.cleanup()
