"""These tests are somewhat brittle to change as they have the exact expected lines hard coded.
This choice was made as it provides better clarity of what the tests are doing.
"""
import operator
import time
import pytest
import tes_lib

FIRST_LINE = "***** Logging Event Store *****"
LAST_LINE = "***** End Of Event Store *****"
SOURCE_1 = "Source1"
SOURCE_2 = "Source2"
EVENT_TYPE_A = "A"
EVENT_TYPE_B = "B"
FIRST_FIELD = "a_field"
FIELD_1 = "field1"
FIELD_2 = "field2"


@pytest.fixture(autouse=True)
def add_events(reset_event_store):
    tes_lib.add_raw_event(SOURCE_1, EVENT_TYPE_A, {FIELD_1: 1, FIELD_2: 19})
    tes_lib.add_raw_event(SOURCE_1, EVENT_TYPE_B, {FIELD_1: 2})
    tes_lib.add_raw_event(SOURCE_2, EVENT_TYPE_A, {FIELD_1: 3})
    tes_lib.add_raw_event(SOURCE_2, EVENT_TYPE_B, {FIELD_1: 4, FIRST_FIELD: "Hi"})
    # Small sleep to make sure the events are added to the store
    time.sleep(0.1)


def test_log_all(caplog):
    """Test the log function with default params"""
    tes_lib.log_event_store()

    expected_log_lines = [
        FIRST_LINE,
        "event_id: 0, {'event_source': 'Source1', 'event_type': 'A', 'field1': 1, 'field2': 19}",
        "event_id: 1, {'event_source': 'Source1', 'event_type': 'B', 'field1': 2}",
        "event_id: 2, {'event_source': 'Source2', 'event_type': 'A', 'field1': 3}",
        "event_id: 3, {'event_source': 'Source2', 'event_type': 'B', 'a_field': 'Hi', 'field1': 4}",
        LAST_LINE,
    ]

    # Check that the logs lines are in the output and in the order we expect
    index = 0
    for line in expected_log_lines:
        assert caplog.messages[index] == line

        index += 1


def test_order_by(caplog):
    """Test ordering by a field"""
    tes_lib.log_event_store(order_by="event_type")

    expected_log_lines = [
        FIRST_LINE,
        "event_id: 0, {'event_source': 'Source1', 'event_type': 'A', 'field1': 1, 'field2': 19}",
        "event_id: 2, {'event_source': 'Source2', 'event_type': 'A', 'field1': 3}",
        "event_id: 1, {'event_source': 'Source1', 'event_type': 'B', 'field1': 2}",
        "event_id: 3, {'event_source': 'Source2', 'event_type': 'B', 'a_field': 'Hi', 'field1': 4}",
        LAST_LINE,
    ]

    # Check that the logs lines are in the output and in the order we expect
    index = 0
    for line in expected_log_lines:
        assert caplog.messages[index] == line

        index += 1


def test_order_by_partial(caplog):
    """Test ordering by a field which only some of the events have"""
    tes_lib.log_event_store(order_by="a_field", order_by_default="~")

    expected_log_lines = [
        FIRST_LINE,
        "event_id: 3, {'event_source': 'Source2', 'event_type': 'B', 'a_field': 'Hi', 'field1': 4}",
        "event_id: 0, {'event_source': 'Source1', 'event_type': 'A', 'field1': 1, 'field2': 19}",
        "event_id: 1, {'event_source': 'Source1', 'event_type': 'B', 'field1': 2}",
        "event_id: 2, {'event_source': 'Source2', 'event_type': 'A', 'field1': 3}",
        LAST_LINE,
    ]

    # Check that the logs lines are in the output and in the order we expect
    index = 0
    for line in expected_log_lines:
        assert caplog.messages[index] == line

        index += 1


def test_fields_to_log(caplog):
    """Test limiting which fields are logged by providing a white list"""
    tes_lib.log_event_store(fields_to_log=["event_source", "field2"])

    expected_log_lines = [
        FIRST_LINE,
        "event_id: 0, {'event_source': 'Source1', 'field2': 19}",
        "event_id: 1, {'event_source': 'Source1'}",
        "event_id: 2, {'event_source': 'Source2'}",
        "event_id: 3, {'event_source': 'Source2'}",
        LAST_LINE,
    ]

    # Check that the logs lines are in the output and in the order we expect
    index = 0
    for line in expected_log_lines:
        assert caplog.messages[index] == line

        index += 1


def test_fields_to_exclude(caplog):
    """Test excluding specific fields from being logged"""
    tes_lib.log_event_store(fields_to_exclude=["field1", "a_field"])

    expected_log_lines = [
        FIRST_LINE,
        "event_id: 0, {'event_source': 'Source1', 'event_type': 'A', 'field2': 19}",
        "event_id: 1, {'event_source': 'Source1', 'event_type': 'B'}",
        "event_id: 2, {'event_source': 'Source2', 'event_type': 'A'}",
        "event_id: 3, {'event_source': 'Source2', 'event_type': 'B'}",
        LAST_LINE,
    ]

    # Check that the logs lines are in the output and in the order we expect
    index = 0
    for line in expected_log_lines:
        assert caplog.messages[index] == line

        index += 1


def test_fields_to_exclude_event_id(caplog):
    """Test excluding the event_id has no effect"""
    tes_lib.log_event_store(fields_to_exclude=["event_id"])

    expected_log_lines = [
        FIRST_LINE,
        "event_id: 0, {'event_source': 'Source1', 'event_type': 'A', 'field1': 1, 'field2': 19}",
        "event_id: 1, {'event_source': 'Source1', 'event_type': 'B', 'field1': 2}",
        "event_id: 2, {'event_source': 'Source2', 'event_type': 'A', 'field1': 3}",
        "event_id: 3, {'event_source': 'Source2', 'event_type': 'B', 'a_field': 'Hi', 'field1': 4}",
        LAST_LINE,
    ]

    # Check that the logs lines are in the output and in the order we expect
    index = 0
    for line in expected_log_lines:
        assert caplog.messages[index] == line

        index += 1


def test_filters(caplog):
    """Test filtering the events logged"""
    tes_lib.log_event_store(filters=[tes_lib.Expectation("field1", operator.ge, 2)])

    expected_log_lines = [
        FIRST_LINE,
        "event_id: 1, {'event_source': 'Source1', 'event_type': 'B', 'field1': 2}",
        "event_id: 2, {'event_source': 'Source2', 'event_type': 'A', 'field1': 3}",
        "event_id: 3, {'event_source': 'Source2', 'event_type': 'B', 'a_field': 'Hi', 'field1': 4}",
        LAST_LINE,
    ]

    # Check that the logs lines are in the output and in the order we expect
    index = 0
    for line in expected_log_lines:
        assert caplog.messages[index] == line

        index += 1


def test_log_full_event_store(caplog):
    """Test the log full store function"""

    tes_lib.expect_event(
        [tes_lib.Expectation("field1", operator.eq, 3)],
        extended_event_debug_on_expectation_failure=False,
    )
    tes_lib.delete_all_matching_events([tes_lib.Expectation("field2", operator.eq, 19)])

    tes_lib.log_full_event_store()

    expected_log_lines = [
        FIRST_LINE,
        "event_id: 0, {'event_source': 'Source1', 'event_state': 'Deleted', 'event_type': 'A', 'field1': 1, 'field2': 19}",
        "event_id: 1, {'event_source': 'Source1', 'event_state': 'Active', 'event_type': 'B', 'field1': 2}",
        "event_id: 2, {'event_source': 'Source2', 'event_state': 'Expected', 'event_type': 'A', 'field1': 3}",
        "event_id: 3, {'event_source': 'Source2', 'event_state': 'Active', 'event_type': 'B', 'a_field': 'Hi', 'field1': 4}",
        LAST_LINE,
    ]

    # Check that the logs lines are in the output and in the order we expect
    index = 0
    for line in expected_log_lines:
        assert caplog.messages[index] == line

        index += 1
