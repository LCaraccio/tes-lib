from tes_lib import TestEvent


class CustomerEvent(TestEvent):
    def __init__(self, event_source, event_type, id, age):
        super().__init__(event_source, event_type)
        self.age = age
        self.id = id


class VendorEvent(TestEvent):
    def __init__(self, event_source, event_type, id, products):
        super().__init__(event_source, event_type)
        self.products = products
        self.id = id


class UserEvent(TestEvent):
    def __init__(self, event_source, event_type, field1, field2, field3):
        super().__init__(event_source, event_type)
        self.field1 = field1
        self.field2 = field2
        self.field3 = field3
