import requests
import pytest
from tes_lib import add_event_web_server
from tes_lib import library_settings
from tes_lib.constants import EVENT_SOURCE_KEY, EVENT_TYPE_KEY
from multiprocessing import Process, Queue


WEBSERVER_IP = "127.0.0.1"
WEBSERVER_PORT = 8080
BASE_URL = "http://{}:{}/".format(WEBSERVER_IP, WEBSERVER_PORT)
ADD_URL = BASE_URL + "add"


@pytest.fixture(autouse=True)
def web_server():
    web_server_process = Process(
        target=add_event_web_server.run_webserver, args=(Queue(), WEBSERVER_IP, WEBSERVER_PORT)
    )
    web_server_process.start()
    library_settings._TesLib.wait_for_webserver(WEBSERVER_IP, WEBSERVER_PORT)
    yield
    web_server_process.terminate()
    web_server_process.join(timeout=3)
    assert not web_server_process.is_alive()


def test_get_correct_url():
    """
    Test performing a GET with the correct url
    """
    response = requests.get(url=BASE_URL + "ready")

    assert response.status_code == requests.codes.ok


def test_get_invalid_url():
    """
    Test performing a GET with an invalid url
    """
    response = requests.get(url=BASE_URL + "hello")

    assert response.status_code == requests.codes.not_found


def test_post_correct_url():
    """
    Test performing a POST with the correct url
    """
    event_params = {EVENT_SOURCE_KEY: "ValidPost", EVENT_TYPE_KEY: "Test"}

    response = requests.post(url=ADD_URL, json=event_params)

    assert response.status_code == requests.codes.ok


def test_post_invalid_url():
    """
    Test performing a POST with an invalid url
    """
    event_params = {EVENT_SOURCE_KEY: "InvalidPost", EVENT_TYPE_KEY: "Test"}

    response = requests.post(url=BASE_URL + "insert", json=event_params)

    assert response.status_code == requests.codes.not_found


def test_post_non_json():
    """
    Test performing a POST with a message that is not json
    """
    response = requests.post(url=ADD_URL, data=b"FlargenJargenWibble")

    assert response.status_code == requests.codes.bad_request
    assert "Failed to decode" in response.text


def test_post_missing_event_source():
    """
    Test performing a POST with the event source field missing
    """
    event_params = {EVENT_TYPE_KEY: "Test"}

    response = requests.post(url=ADD_URL, json=event_params)

    assert response.status_code == requests.codes.bad_request
    assert EVENT_SOURCE_KEY in response.text


def test_post_missing_event_type():
    """
    Test performing a POST with the event type field missing
    """
    event_params = {EVENT_SOURCE_KEY: "InvalidPost"}

    response = requests.post(url=ADD_URL, json=event_params)

    assert response.status_code == requests.codes.bad_request
    assert EVENT_TYPE_KEY in response.text
