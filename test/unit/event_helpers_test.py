import pytest

import tes_lib
import operator
from tes_lib import messages, TestEvent
from tes_lib.constants import NO_MATCHING_EVENT_FOUND, EVENT_SOURCE_KEY
from tes_lib.errors import (
    EventNotFoundError,
    ExpectationError,
    TesLibError,
    ExtendedEventNotFoundError,
)
from tes_lib.expectation import PartialMatch, FailedExpectation, ExpectationFailureType


class PersonEvent(TestEvent):
    def __init__(self, event_source, event_type, age, height, name):
        super().__init__(event_source, event_type)
        self.age = age
        self.height = height
        self.name = name


def test_get_event(mocker):
    """
    Test getting an event from the event store
    """
    expected_event = [{"event_source": "TestHarness", "event_type": "TestEventA"}]

    tes_lib_instance_mock = mocker.patch("tes_lib.event_helpers._TesLibInstance")
    instance_mock = mocker.MagicMock()
    tes_lib_instance_mock.get_instance.return_value = instance_mock

    parent_conn_mock = mocker.MagicMock()
    parent_conn_mock.recv.return_value = expected_event
    instance_mock.parent_conn = parent_conn_mock

    expectations = [tes_lib.Expectation("event_source", operator.eq, "TestHarness")]
    event = tes_lib.get_event(expectations, extended_event_debug_on_expectation_failure=False)

    assert event == expected_event
    instance_mock.queue.put.assert_called_once_with(mocker.ANY)

    args, _ = instance_mock.queue.put.call_args
    message = args[0]

    assert isinstance(message, messages.ExpectEventMessage)


def test_get_all_events(mocker):
    """
    Test getting all events from the event store
    """
    expected_events = [
        {"event_source": "TestHarness", "event_type": "TestEventA"},
        {"event_source": "TestHarness", "event_type": "TestEventB"},
    ]

    tes_lib_instance_mock = mocker.patch("tes_lib.event_helpers._TesLibInstance")
    instance_mock = mocker.MagicMock()
    tes_lib_instance_mock.get_instance.return_value = instance_mock

    parent_conn_mock = mocker.MagicMock()
    parent_conn_mock.recv.return_value = expected_events
    instance_mock.parent_conn = parent_conn_mock

    events = tes_lib.get_all_events()

    assert events == expected_events
    instance_mock.queue.put.assert_called_once_with(mocker.ANY)

    args, _ = instance_mock.queue.put.call_args
    message = args[0]

    assert isinstance(message, messages.GetAllEventsMessage)


def test_expect_event_event_found(mocker):
    """
    Test expecting an event from the event store when the event is found

    Expectations:
    - Expect event
    - When found delete event
    - Return event
    """
    expected_event = {"event_source": "TestHarness", "event_type": "TestEventA", "TestField": 5}

    tes_lib_instance_mock = mocker.patch("tes_lib.event_helpers._TesLibInstance")
    instance_mock = mocker.MagicMock()
    tes_lib_instance_mock.get_instance.return_value = instance_mock

    parent_conn_mock = mocker.MagicMock()
    parent_conn_mock.poll.return_value = True
    parent_conn_mock.recv.return_value = expected_event
    instance_mock.parent_conn = parent_conn_mock

    expectations = [tes_lib.Expectation("TestField", operator.eq, 5)]

    event = tes_lib.expect_event(expectations, extended_event_debug_on_expectation_failure=False)

    assert event == expected_event
    instance_mock.queue.put.assert_called()

    call_args = instance_mock.queue.put.call_args_list
    assert len(call_args) == 2

    args, _ = call_args[0]
    message = args[0]

    assert isinstance(message, messages.ExpectEventMessage)
    assert message.expectations == expectations

    args, _ = call_args[1]
    message = args[0]

    assert isinstance(message, messages.RemoveExpectedEventMessage)
    assert message.event == event


def test_expect_event_event_found_extended_debug(mocker):
    """
    Test expecting an event from the event store when the event is found where the extended
    event debug is enabled

    Expectations:
    - Expect event
    - When found delete event
    - Return event
    """
    expected_event = {"event_source": "TestHarness", "event_type": "TestEventA", "TestField": 5}

    tes_lib_instance_mock = mocker.patch("tes_lib.event_helpers._TesLibInstance")
    instance_mock = mocker.MagicMock()
    tes_lib_instance_mock.get_instance.return_value = instance_mock

    parent_conn_mock = mocker.MagicMock()
    parent_conn_mock.poll.return_value = True
    parent_conn_mock.recv.return_value = (expected_event, [])
    instance_mock.parent_conn = parent_conn_mock

    expectations = [
        tes_lib.Expectation(EVENT_SOURCE_KEY, operator.eq, "unittest"),
        tes_lib.Expectation("TestField", operator.eq, 5),
    ]

    event = tes_lib.expect_event(
        expectations,
        extended_event_debug_on_expectation_failure=True,
    )

    assert event == expected_event
    instance_mock.queue.put.assert_called()

    call_args = instance_mock.queue.put.call_args_list
    assert len(call_args) == 3

    args, _ = call_args[0]
    message = args[0]

    assert isinstance(message, messages.ExpectEventExtendedDebugMessage)
    assert message.expectations == expectations

    args, _ = call_args[1]
    message = args[0]

    assert isinstance(message, messages.RemoveExpectedEventMessage)
    assert message.event == event


def test_expect_event_event_not_found(mocker):
    """
    Test expecting an event from the event store when the event is not found

    Expectations:
    - Expect event
    - Don't find event and time out
    - Return None
    """
    tes_lib_instance_mock = mocker.patch("tes_lib.event_helpers._TesLibInstance")
    instance_mock = mocker.MagicMock()
    tes_lib_instance_mock.get_instance.return_value = instance_mock

    parent_conn_mock = mocker.MagicMock()
    parent_conn_mock.poll.return_value = True
    parent_conn_mock.recv.return_value = NO_MATCHING_EVENT_FOUND
    instance_mock.parent_conn = parent_conn_mock

    expectations = [tes_lib.Expectation("TestField", operator.eq, 5)]

    def on_fail():
        pass

    # Set the timeout and poll_interval such that we will expect the event 10 times
    with pytest.raises(EventNotFoundError):
        _ = tes_lib.expect_event(
            expectations,
            maximum_poll_time=1,
            poll_interval=0.1,
            on_failure=on_fail,
            extended_event_debug_on_expectation_failure=False,
        )

    # the initial call plus 10 more before we hit the max timeout
    assert instance_mock.queue.put.call_count == 11


def test_expect_event_event_not_found_extended_debug(mocker):
    """
    Test expecting an event from the event store when the event is not found where the extended
    event debug is enabled

    Expectations:
    - Expect event
    - Don't find event and time out
    - Return None
    """
    tes_lib_instance_mock = mocker.patch("tes_lib.event_helpers._TesLibInstance")
    instance_mock = mocker.MagicMock()
    tes_lib_instance_mock.get_instance.return_value = instance_mock

    parent_conn_mock = mocker.MagicMock()
    parent_conn_mock.poll.return_value = True

    failing_expectation = tes_lib.Expectation("TestField", operator.eq, 5)
    parent_conn_mock.recv.return_value = (
        NO_MATCHING_EVENT_FOUND,
        [
            PartialMatch(
                {tes_lib.constants.EVENT_ID_KEY: 1},
                [FailedExpectation(failing_expectation, ExpectationFailureType.FIELD_NOT_FOUND)],
            )
        ],
    )
    instance_mock.parent_conn = parent_conn_mock

    expectations = [
        tes_lib.Expectation(EVENT_SOURCE_KEY, operator.eq, "unittest"),
        failing_expectation,
    ]

    def on_fail():
        pass

    # Set the timeout and poll_interval such that we will expect the event 10 times
    with pytest.raises(ExtendedEventNotFoundError):
        _ = tes_lib.expect_event(
            expectations,
            maximum_poll_time=1,
            poll_interval=0.1,
            on_failure=on_fail,
            extended_event_debug_on_expectation_failure=True,
        )

    # the initial call plus 10 more before we hit the max timeout
    assert instance_mock.queue.put.call_count == 11


def test_expect_event_no_response(mocker):
    """
    Test expecting an event from the event store where the store does not reply

    Expectations:
    - Expect event
    - Don't get a reply and give up
    """
    tes_lib_instance_mock = mocker.patch("tes_lib.event_helpers._TesLibInstance")
    instance_mock = mocker.MagicMock()
    tes_lib_instance_mock.get_instance.return_value = instance_mock

    parent_conn_mock = mocker.MagicMock()
    parent_conn_mock.poll.return_value = False
    instance_mock.parent_conn = parent_conn_mock

    expectations = [tes_lib.Expectation("TestField", operator.eq, 5)]

    with pytest.raises(TesLibError):
        _ = tes_lib.expect_event(expectations, maximum_poll_time=1, poll_interval=0.1)


def test_add_event(mocker):
    """
    Test adding an event to the event store

    This should make a rest request to the add event webserver
    """
    mock_response = mocker.MagicMock()
    mock_response.status_code = 200

    requests_mock_post = mocker.patch("tes_lib.event_helpers.requests.post")
    requests_mock_post.return_value = mock_response

    event_source = "TestHarness"
    event_type = "TestEventA"
    person_event = PersonEvent(event_source, event_type, 19, 193, "bob")

    expected_url = "http://127.0.0.1:8080/add"

    tes_lib.add_event(person_event)

    requests_mock_post.assert_called_once_with(url=expected_url, json=vars(person_event))


def test_add_raw_event(mocker):
    """
    Test adding a raw event to the event store

    This should make a rest request to the add event webserver
    """
    mock_response = mocker.MagicMock()
    mock_response.status_code = 200

    requests_mock_post = mocker.patch("tes_lib.event_helpers.requests.post")
    requests_mock_post.return_value = mock_response

    event_source = "TestHarness"
    event_type = "TestEventA"
    additional_params = {"age": 19, "height": 193, "name": "bob"}

    expected_dict = {
        "event_source": event_source,
        "event_type": event_type,
        "age": 19,
        "height": 193,
        "name": "bob",
    }

    expected_url = "http://127.0.0.1:8080/add"

    tes_lib.add_raw_event(event_source, event_type, additional_params)

    requests_mock_post.assert_called_once_with(url=expected_url, json=expected_dict)


def test_expectation_validation():

    for expectation in [None, []]:
        for function in [
            tes_lib.expect_event,
            tes_lib.dont_expect_event,
            tes_lib.get_event,
        ]:
            try:
                function(expectation, 1)
                pytest.fail("Expected expectation was not raised")
            except ExpectationError:
                pass

        for function in [
            tes_lib.get_all_matching_events,
            tes_lib.delete_all_matching_events,
        ]:
            try:
                function(expectation)
                pytest.fail("Expected expectation was not raised")
            except ExpectationError:
                pass
