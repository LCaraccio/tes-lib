import tes_lib
import operator
import pytest


def test_expectation():
    """
    Test Expectation instantiation
    """
    field_name = "TestField"
    comparison_operator = operator.eq
    comparison_value = 17
    expectation = tes_lib.Expectation(field_name, comparison_operator, comparison_value)

    assert expectation.event_field_name == field_name
    assert expectation.comparison_function == comparison_operator
    assert expectation.comparison_value == comparison_value


def test_expectation_comparison_function_too_many_arguments():
    """
    Test an Expectation where the comparison function takes too many arguments
    """

    def comparison_function(a, b, c):
        pass

    field_name = "TestField"
    comparison_value = 17

    with pytest.raises(TypeError):
        _ = tes_lib.Expectation(field_name, comparison_function, comparison_value)


def test_expectation_comparison_function_too_few_arguments():
    """
    Test an Expectation where the comparison function takes too few arguments
    """

    def comparison_function():
        pass

    field_name = "TestField"
    comparison_value = 17

    with pytest.raises(TypeError):
        _ = tes_lib.Expectation(field_name, comparison_function, comparison_value)


def test_expectation_comparison_function_takes_one_argument():
    """
    Test an Expectation where the comparison function takes one argument but a comparison value
    has been provided
    """

    def comparison_function(a):
        pass

    field_name = "TestField"
    comparison_value = 17

    with pytest.raises(TypeError):
        _ = tes_lib.Expectation(field_name, comparison_function, comparison_value)


def test_expectation_comparison_function_takes_two_arguments():
    """
    Test an Expectation where the comparison function takes two arguments but no comparison
    value has been provided
    """

    def comparison_function(a, b):
        pass

    field_name = "TestField"

    with pytest.raises(TypeError):
        _ = tes_lib.Expectation(field_name, comparison_function)


def test_expectation_comparison_function_using_builtin():
    """
    Test an Expectation where the comparison function in a builtin. Previously this would
    have failed due to the method being used to get the number of arguments the function
    takes.
    """
    field_name = "TestField"

    try: 
        _ = tes_lib.Expectation(field_name, str.endswith, "potato")
    except Exception as exc:
        pytest.fail(f"Unexpected error: {exc}")
