"""
Tests for the test event store

These tests could have tested the event store directly, I have chosen to go through
the message queue layer as I think this will produce more representative tests.

As such these tests are more process than unit tests.
"""

import operator
import tes_lib
import pytest
from tes_lib import test_event_store
from tes_lib import messages
from tes_lib.constants import NO_MATCHING_EVENT_FOUND, EVENT_SOURCE_KEY, EVENT_TYPE_KEY
from tes_lib.event_helpers import wait_for_response
from tes_lib.expectation import PartialMatch, FailedExpectation, ExpectationFailureType
from multiprocessing import Queue, Pipe, Process


@pytest.fixture
def event_queue():
    return Queue()


@pytest.fixture
def pipe_conns():
    parent_conn, child_conn = Pipe()
    return [parent_conn, child_conn]


@pytest.fixture
def event_store(event_queue, pipe_conns):
    # Start the event store process
    event_store_process = Process(
        target=test_event_store.event_store_run_loop, args=(event_queue, pipe_conns[1])
    )
    event_store_process.start()
    yield
    event_queue.put(messages.ShutdownMessage())
    event_store_process.join(timeout=3)
    assert not event_store_process.is_alive()


def test_start_stop():
    """
    Test starting and stopping the test event store process
    """
    # Start the event store process
    queue = Queue()
    _, child_conn = Pipe()
    event_store_process = Process(
        target=test_event_store.event_store_run_loop, args=(queue, child_conn)
    )
    event_store_process.start()
    assert event_store_process.is_alive()

    queue.put(messages.ShutdownMessage())
    event_store_process.join(timeout=3)
    assert not event_store_process.is_alive()


@pytest.mark.usefixtures("event_store")
def test_add_get(event_queue, pipe_conns):
    """
    Test adding an event to the store and getting the events in the store
    """
    parent_conn = pipe_conns[0]

    event_dict = {
        EVENT_SOURCE_KEY: "TestHarness",
        EVENT_TYPE_KEY: "TestEventC",
        "animal": "Cat",
        "name": "TinyCat",
        "colour": "Black",
    }

    event_queue.put(messages.AddEventMessage(event_dict))
    event_queue.put(messages.GetAllEventsMessage())

    events = wait_for_response("test_framework", parent_conn)

    assert len(events) == 1

    returned_event = events[0]
    for key in event_dict:
        assert event_dict[key] == returned_event[key]


@pytest.mark.usefixtures("event_store")
def test_get_full(event_queue, pipe_conns):
    """
    Test getting the full event store, including those events that have been deleted or expected
    """
    parent_conn = pipe_conns[0]

    messages_to_add = 25
    for i in range(messages_to_add):
        event_dict = {
            EVENT_SOURCE_KEY: "TestHarness",
            EVENT_TYPE_KEY: "TestEventC",
            "animal": "Cat",
            "name": "TinyCat",
            "colour": "Black",
            "age": i,
        }

        event_queue.put(messages.AddEventMessage(event_dict))

    # Expect an event
    expectations = [tes_lib.Expectation("age", operator.eq, 12)]
    event_queue.put(messages.ExpectEventMessage(expectations))

    returned_event = wait_for_response("test_framework", parent_conn)
    assert returned_event != NO_MATCHING_EVENT_FOUND

    # Delete the expected event
    event_queue.put(messages.RemoveExpectedEventMessage(returned_event))

    # Delete a number of events
    event_queue.put(
        messages.DeleteMatchingEventsMessage([tes_lib.Expectation("age", operator.le, 3)])
    )
    events_removed = wait_for_response("test_framework", parent_conn)

    assert events_removed == 4

    # Get the full list of events
    event_queue.put(messages.GetFullEventLogMessage())
    full_event_log = wait_for_response("test_framework", parent_conn)

    assert len(full_event_log) == messages_to_add


@pytest.mark.usefixtures("event_store")
def test_get_event(event_queue, pipe_conns):
    """
    Test getting an event from the event store
    """
    parent_conn = pipe_conns[0]

    messages_to_add = 25
    for i in range(messages_to_add):
        event_dict = {
            EVENT_SOURCE_KEY: "TestHarness",
            EVENT_TYPE_KEY: "TestEventC",
            "animal": "Cat",
            "name": "TinyCat",
            "colour": "Black",
            "age": i,
        }

        event_queue.put(messages.AddEventMessage(event_dict))

    # Get an event
    expectations = [tes_lib.Expectation("animal", operator.eq, "Cat")]
    event_queue.put(messages.ExpectEventMessage(expectations))

    returned_event = wait_for_response("test_framework", parent_conn)
    assert returned_event != NO_MATCHING_EVENT_FOUND

    # Get the full list of events to make sure it has not been removed
    event_queue.put(messages.GetFullEventLogMessage())
    full_event_log = wait_for_response("test_framework", parent_conn)

    assert len(full_event_log) == messages_to_add


@pytest.mark.usefixtures("event_store")
def test_get_all_matching(event_queue, pipe_conns):
    """
    Test getting all the events in the store that match a set criteria
    """
    parent_conn = pipe_conns[0]

    for i in range(23):
        event_dict = {
            EVENT_SOURCE_KEY: "TestHarness",
            EVENT_TYPE_KEY: "TestEventC",
            "animal": "Cat",
            "name": "TinyCat",
            "colour": "Black",
            "age": i,
        }

        event_queue.put(messages.AddEventMessage(event_dict))

    target_age = 16
    expectations = [tes_lib.Expectation("age", operator.ge, target_age)]
    event_queue.put(messages.GetAllMatchingEventsMessage(expectations))

    events = wait_for_response("test_framework", parent_conn)

    assert len(events) == 7

    for event in events:
        assert event["age"] >= target_age


@pytest.mark.usefixtures("event_store")
def test_expect_event(event_queue, pipe_conns):
    """
    Test expecting an event is in the store
    """
    parent_conn = pipe_conns[0]

    # Add a number of events
    event_one = {
        EVENT_SOURCE_KEY: "TestHarness",
        EVENT_TYPE_KEY: "TestEventC",
        "animal": "Cat",
        "name": "TinyCat",
        "colour": "Black",
    }

    event_queue.put(messages.AddEventMessage(event_one))

    event_two = {
        EVENT_SOURCE_KEY: "TestHarness",
        EVENT_TYPE_KEY: "TestEventC",
        "animal": "Dog",
        "name": "Buster",
        "colour": "Brown",
    }

    event_queue.put(messages.AddEventMessage(event_two))

    event_three = {
        EVENT_SOURCE_KEY: "TestHarness",
        EVENT_TYPE_KEY: "TestEventC",
        "animal": "Fish",
        "name": "Glub",
        "colour": "Orange",
    }

    event_queue.put(messages.AddEventMessage(event_three))

    # Expect an event that is not in the store
    expectations = [tes_lib.Expectation("animal", operator.eq, "Monkey")]
    event_queue.put(messages.ExpectEventMessage(expectations))

    returned_event = wait_for_response("test_framework", parent_conn)

    assert returned_event == NO_MATCHING_EVENT_FOUND

    # Expect an event that is in the store
    expectations = [tes_lib.Expectation("animal", operator.eq, "Cat")]
    event_queue.put(messages.ExpectEventMessage(expectations))

    returned_event = wait_for_response("test_framework", parent_conn)

    for key in event_one:
        assert event_one[key] == returned_event[key]


@pytest.mark.usefixtures("event_store")
def test_expect_event_no_comparison_value(event_queue, pipe_conns):
    """
    Test expecting an event is in the store when the comparison function doesn't
    take a value to compare against
    """
    parent_conn = pipe_conns[0]

    # Add a number of events
    event_one = {
        EVENT_SOURCE_KEY: "TestHarness",
        EVENT_TYPE_KEY: "TestEventC",
        "animal": "Cat",
        "name": "TinyCat",
        "colour": "Black",
        "aggressive": True,
    }

    event_queue.put(messages.AddEventMessage(event_one))

    event_two = {
        EVENT_SOURCE_KEY: "TestHarness",
        EVENT_TYPE_KEY: "TestEventC",
        "animal": "Dog",
        "name": "Buster",
        "colour": "Brown",
        "aggressive": False,
    }

    event_queue.put(messages.AddEventMessage(event_two))

    event_three = {
        EVENT_SOURCE_KEY: "TestHarness",
        EVENT_TYPE_KEY: "TestEventC",
        "animal": "Fish",
        "name": "Glub",
        "colour": "Orange",
        "aggressive": True,
    }

    event_queue.put(messages.AddEventMessage(event_three))

    # Expect an event that is in the store
    expectations = [tes_lib.Expectation("aggressive", operator.truth)]
    event_queue.put(messages.ExpectEventMessage(expectations))

    returned_event = wait_for_response("test_framework", parent_conn)

    for key in event_one:
        assert event_one[key] == returned_event[key]


@pytest.mark.usefixtures("event_store")
def test_expect_event_extended_debug(event_queue, pipe_conns):
    """
    Test expecting an event is in the store with extended debug turned on for failures
    """
    parent_conn = pipe_conns[0]

    # Add a number of events
    event_one = {
        EVENT_SOURCE_KEY: "TestHarness",
        EVENT_TYPE_KEY: "TestEventC",
        "animal": "Cat",
        "name": "TinyCat",
        "colour": "Black",
    }

    event_queue.put(messages.AddEventMessage(event_one))

    event_two = {
        EVENT_SOURCE_KEY: "TestHarness",
        EVENT_TYPE_KEY: "TestEventC",
        "animal": "Dog",
        "name": "Buster",
        "colour": "Brown",
    }

    event_queue.put(messages.AddEventMessage(event_two))

    event_three = {
        EVENT_SOURCE_KEY: "TestHarness",
        EVENT_TYPE_KEY: "TestEventC",
        "animal": "Fish",
        "name": "Glub",
        "colour": "Orange",
    }

    event_queue.put(messages.AddEventMessage(event_three))

    # Expect an event that is not in the store
    expectations = [
        tes_lib.Expectation(EVENT_SOURCE_KEY, operator.eq, "TestHarness"),
        tes_lib.Expectation("animal", operator.eq, "Monkey"),
    ]
    event_queue.put(
        messages.ExpectEventExtendedDebugMessage(
            expectations, [tes_lib.Expectation(EVENT_SOURCE_KEY, operator.eq, "TestHarness")]
        )
    )

    result = wait_for_response("test_framework", parent_conn)
    returned_event = result[0]
    failures = result[1]

    assert returned_event == NO_MATCHING_EVENT_FOUND
    assert failures != []

    # Expect an event that is in the store
    expectations = [
        tes_lib.Expectation(EVENT_SOURCE_KEY, operator.eq, "TestHarness"),
        tes_lib.Expectation("animal", operator.eq, "Cat"),
    ]
    event_queue.put(
        messages.ExpectEventExtendedDebugMessage(
            expectations, [tes_lib.Expectation(EVENT_SOURCE_KEY, operator.eq, "TestHarness")]
        )
    )

    result = wait_for_response("test_framework", parent_conn)
    returned_event = result[0]
    failures = result[1]

    for key in event_one:
        assert event_one[key] == returned_event[key]

    assert failures == []


@pytest.mark.usefixtures("event_store")
def test_expect_event_none_type_comparison(event_queue, pipe_conns):
    """
    Test expecting an event when a similar event is present and has None as the value for one of the fields
    """
    parent_conn = pipe_conns[0]

    # Add a number of events
    event_one = {
        EVENT_SOURCE_KEY: "TestHarness",
        EVENT_TYPE_KEY: "TestEventC",
        "animal": "Cat",
        "name": "TinyCat",
        "colour": "Black",
        "age": None,
    }

    event_queue.put(messages.AddEventMessage(event_one))

    event_two = {
        EVENT_SOURCE_KEY: "TestHarness",
        EVENT_TYPE_KEY: "TestEventC",
        "animal": "Dog",
        "name": "Buster",
        "colour": "Brown",
        "age": 5,
    }

    event_queue.put(messages.AddEventMessage(event_two))

    # Expect an event that has some fields that are None in a similar event
    expectations = [tes_lib.Expectation("age", operator.gt, 3)]
    event_queue.put(messages.ExpectEventMessage(expectations))

    returned_event = wait_for_response("test_framework", parent_conn)

    for key in event_two:
        assert event_two[key] == returned_event[key]


@pytest.mark.usefixtures("event_store")
def test_delete(event_queue, pipe_conns):
    """
    Test deleting an event from the store
    """
    parent_conn = pipe_conns[0]

    # Add a number of events
    event_one = {
        EVENT_SOURCE_KEY: "TestHarness",
        EVENT_TYPE_KEY: "TestEventD",
        "part_no": 245,
        "name": "WheelRim",
        "colour": "Silver",
    }

    event_queue.put(messages.AddEventMessage(event_one))

    event_two = {
        EVENT_SOURCE_KEY: "TestHarness",
        EVENT_TYPE_KEY: "TestEventD",
        "part_no": 121,
        "name": "Door",
        "colour": "Red",
    }

    event_queue.put(messages.AddEventMessage(event_two))

    # Note this event is not in the store
    event_three = {
        EVENT_SOURCE_KEY: "TestHarness",
        EVENT_TYPE_KEY: "TestEventD",
        "part_no": 414,
        "name": "Windscreen",
        "colour": "Clear",
    }

    # Try to delete an event that is in the store
    event_queue.put(messages.RemoveExpectedEventMessage(event_three))

    # Make sure there are still 2 events in the store
    event_queue.put(messages.GetAllEventsMessage())
    events = wait_for_response("test_framework", parent_conn)
    assert len(events) == 2

    # Try to delete an event that is not in the store
    event_queue.put(messages.RemoveExpectedEventMessage(events[1]))

    # Make sure there is only 1 event in the store
    event_queue.put(messages.GetAllEventsMessage())
    events = wait_for_response("test_framework", parent_conn)
    assert len(events) == 1

    returned_event = events[0]
    for key in event_one:
        assert event_one[key] == returned_event[key]


@pytest.mark.usefixtures("event_store")
def test_delete_matching(event_queue, pipe_conns):
    """
    Test deleting matching events from the store
    """
    parent_conn = pipe_conns[0]

    # Add a number of events
    wheel_events = 6
    for i in range(wheel_events):
        event = {
            EVENT_SOURCE_KEY: "TestHarness",
            EVENT_TYPE_KEY: "TestEventD",
            "part_no": i,
            "name": "WheelRim",
            "colour": "Silver",
        }

        event_queue.put(messages.AddEventMessage(event))

    # Add a second set with a different name
    door_events = 3
    for i in range(door_events):
        event = {
            EVENT_SOURCE_KEY: "TestHarness",
            EVENT_TYPE_KEY: "TestEventD",
            "part_no": i,
            "name": "Door",
            "colour": "Red",
        }

        event_queue.put(messages.AddEventMessage(event))

    # Make sure all the events are in the store
    event_queue.put(messages.GetAllEventsMessage())
    events = wait_for_response("test_framework", parent_conn)
    assert len(events) == wheel_events + door_events

    # Delete only those events with a part number of 2
    expectations = [tes_lib.Expectation("part_no", operator.eq, 2)]
    event_queue.put(messages.DeleteMatchingEventsMessage(expectations))

    expected_events_removed = 2
    actual_events_removed = wait_for_response("test_framework", parent_conn)
    assert expected_events_removed == actual_events_removed

    event_queue.put(messages.GetAllEventsMessage())
    events = wait_for_response("test_framework", parent_conn)
    assert len(events) == wheel_events + door_events - expected_events_removed


@pytest.mark.usefixtures("event_store")
def test_reset(event_queue, pipe_conns):
    """
    Test resetting the store
    """
    parent_conn = pipe_conns[0]

    # Add a number of events
    event_one = {
        EVENT_SOURCE_KEY: "TestHarness",
        EVENT_TYPE_KEY: "TestEventD",
        "part_no": 245,
        "name": "WheelRim",
        "colour": "Silver",
    }

    event_queue.put(messages.AddEventMessage(event_one))

    event_two = {
        EVENT_SOURCE_KEY: "TestHarness",
        EVENT_TYPE_KEY: "TestEventD",
        "part_no": 121,
        "name": "Door",
        "colour": "Red",
    }

    event_queue.put(messages.AddEventMessage(event_two))

    # Make sure there are 2 events in the store
    event_queue.put(messages.GetAllEventsMessage())
    events = wait_for_response("test_framework", parent_conn)
    assert len(events) == 2

    # Reset the store
    event_queue.put(messages.ResetMessage())
    _ = wait_for_response("test_framework", parent_conn)

    # Make sure there are no events in the store
    event_queue.put(messages.GetAllEventsMessage())
    events = wait_for_response("test_framework", parent_conn)
    print(events)
    assert len(events) == 0


def test_extended_expect_event():
    event_store = test_event_store._TestEventStore()

    input_event = {
        EVENT_SOURCE_KEY: "unit_test",
        EVENT_TYPE_KEY: "unit_test",
        "comparison_field": "1",
        "none_field": None,
    }

    partial_match_filters = [
        tes_lib.Expectation(EVENT_SOURCE_KEY, operator.eq, "unit_test"),
        tes_lib.Expectation(EVENT_TYPE_KEY, operator.eq, "unit_test"),
    ]

    test_cases = [
        {
            "name": "field_not_found_failure",
            "expectations": [
                tes_lib.Expectation(EVENT_SOURCE_KEY, operator.eq, "unit_test"),
                tes_lib.Expectation("other_field", operator.eq, "2"),
            ],
            "expected_failures": [
                PartialMatch(
                    input_event,
                    [
                        FailedExpectation(
                            tes_lib.Expectation("other_field", operator.eq, "2"),
                            ExpectationFailureType.FIELD_NOT_FOUND,
                        )
                    ],
                ),
            ],
        },
        {
            "name": "value_comparison_failure",
            "expectations": [
                tes_lib.Expectation(EVENT_SOURCE_KEY, operator.eq, "unit_test"),
                tes_lib.Expectation("comparison_field", operator.eq, "2"),
            ],
            "expected_failures": [
                PartialMatch(
                    input_event,
                    [
                        FailedExpectation(
                            tes_lib.Expectation("comparison_field", operator.eq, "2"),
                            ExpectationFailureType.COMPARISON_FAILURE,
                            input_event["comparison_field"],
                        )
                    ],
                ),
            ],
        },
        {
            "name": "type_mismatch_int",
            "expectations": [
                tes_lib.Expectation(EVENT_SOURCE_KEY, operator.eq, "unit_test"),
                tes_lib.Expectation("comparison_field", operator.eq, 1),
            ],
            "expected_failures": [
                PartialMatch(
                    input_event,
                    [
                        FailedExpectation(
                            tes_lib.Expectation("comparison_field", operator.eq, 1),
                            ExpectationFailureType.COMPARISON_FAILURE,
                            input_event["comparison_field"],
                        )
                    ],
                ),
            ],
        },
        {
            "name": "type_mismatch_float",
            "expectations": [
                tes_lib.Expectation(EVENT_SOURCE_KEY, operator.eq, "unit_test"),
                tes_lib.Expectation("comparison_field", operator.eq, 1.0),
            ],
            "expected_failures": [
                PartialMatch(
                    input_event,
                    [
                        FailedExpectation(
                            tes_lib.Expectation("comparison_field", operator.eq, 1.0),
                            ExpectationFailureType.COMPARISON_FAILURE,
                            input_event["comparison_field"],
                        )
                    ],
                ),
            ],
        },
        {
            "name": "type_is_none",
            "expectations": [
                tes_lib.Expectation(EVENT_SOURCE_KEY, operator.eq, "unit_test"),
                tes_lib.Expectation("none_field", operator.gt, 1.0),
            ],
            "expected_failures": [
                PartialMatch(
                    input_event,
                    [
                        FailedExpectation(
                            tes_lib.Expectation("none_field", operator.gt, 1.0),
                            ExpectationFailureType.COMPARISON_FAILURE,
                            input_event["none_field"],
                        )
                    ],
                ),
            ],
        },
        {
            "name": "single_comparison_field",
            "expectations": [
                tes_lib.Expectation(EVENT_SOURCE_KEY, operator.eq, "unit_test"),
                tes_lib.Expectation("none_field", operator.truth),
            ],
            "expected_failures": [
                PartialMatch(
                    input_event,
                    [
                        FailedExpectation(
                            tes_lib.Expectation("none_field", operator.truth),
                            ExpectationFailureType.COMPARISON_FAILURE,
                            input_event["none_field"],
                        )
                    ],
                ),
            ],
        },
    ]

    for test_case in test_cases:

        event_store.reset()
        event_store.add(input_event)

        result = event_store.extended_expect_event(test_case["expectations"], partial_match_filters)
        assert result[0] == NO_MATCHING_EVENT_FOUND, test_case["name"]
        failures = result[1]

        assert len(failures) == len(test_case["expected_failures"]), test_case["name"]

        for i in range(len(test_case["expected_failures"])):
            assert failures[i].event == test_case["expected_failures"][i].event, test_case["name"]
            assert str(failures[i].expectation_failures) == str(
                test_case["expected_failures"][i].expectation_failures
            ), test_case["name"]
